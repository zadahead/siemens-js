import { Btn, Div, El, H2, Input, State } from '../Elements';

import './Calc.css';
/*
 <div class="calc">
        <div class="top">
            <div class="screen"></div>
            <div class="cancel"></div>
        </div>
        <div class="body">
            <div class="digits"></div>
            <div class="operators"></div>
        </div>
    </div>
*/

export const Calc = () => {
    const initialState = '0.';
    const initialCalcState = { value: '0', oper: null };

    const screenState = new State(initialState);
    const calcState = new State(initialCalcState);

    const handleAction = (e) => {
        const key = e.target.innerText;

        switch (key) {
            //operators
            case '/':
            case '*':
            case '-':
            case '+':
                handleOperator(key);
                break;

            //special
            case 'C':
                clear();
                calcState.setValue(initialCalcState);
                break;

            case '.':
                if(!screenState.value.includes('.')) {
                    handleDigits(key);
                }
                break;

            case '=':
                handleSum();
                break;

            //digits
            default:
                handleDigits(key);
                break;
        }
        
    }

    const handleSum = () => {
        const { value, oper } = calcState.value;
        if(oper) {
            const sum = eval(`${value} ${oper} ${screenState.value}`)
            screenState.setValue(sum);
            calcState.setValue(initialCalcState);
        }
    }

    const handleOperator = (key) => {
        handleSum();

        calcState.setValue({
            value: screenState.value,
            oper: key
        })
        clear();
    }

    const handleDigits = (key) => {
        if(screenState.value === initialState) { empty() }

        screenState.setValue(screenState.value + key);
    }

    const clear = () => {
        screenState.setValue(initialState);
    }

    const empty = () => {
        screenState.setValue('');
    }

    const digits = [
        '7', '8', '9',
        '4', '5', '6',
        '1', '2', '3',
        '0', '.', '='
    ]

    const digitsMap = digits.map(i => Btn(i)(handleAction));

    const operators = ['/', '*', '-', '+'];

    const operatorsMap = operators.map(i => Btn(i)(handleAction));

    return (
        Div('calc')(
            Div('top')(
                Div('screen')(
                    H2(initialState).bind(screenState, (el, state) => {
                        el.content(state);
                    }),
                    El('h5').bind(calcState, (el, state) => {
                        if(state.oper ) {
                            return el.content(`${state.oper} ${state.value}`)
                        }
                        return el.content('');
                    })
                ),
                Div('cancel')(
                    Btn('C')(handleAction)
                ),
            ),
            Div('body')(
                Div('digits')(digitsMap),
                Div('operators')(operatorsMap),
            )
        )
    )
}