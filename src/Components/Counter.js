import { Div, H1, Btn, State } from '../Elements';


export const Counter = () => {
    const counterState = new State(0);

    const handleAdd = () => {
        console.log('handleAdd');
        counterState.setValue(counterState.value + 1);
    }

    const handleSub = () => {
        console.log('handleAdd');
        counterState.setValue(counterState.value - 1);
    }

    return (
        Div(
            H1('Count: ').bind(counterState, (el, state) => {
                el.content(`Count: ${state}`);
            }),
            Btn('Add')(handleAdd),
            Btn('Sub')(handleSub)
        )
    )

}