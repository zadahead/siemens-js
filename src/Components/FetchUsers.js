/*
    1) create functional component FetchUsers()
    2) will load with axios list of users GET 'http://localhost:4000/users'
    3) when load, this will render the list to the page 
    4) while loading.. show "loading... " insicator on screen
*/
import axios from 'axios';
import { Div, H1, H2, State  } from "../Elements"

export const FetchUsers = () => {
    //add state for users list
    const listState = new State(null);

    //fetch 
    setTimeout(() => {
        axios.get('http://localhost:4000/users')
            .then(resp => {
                listState.setValue(resp.data);
            })
    }, 3000);

    return (
        Div('box')(
            Div('list')(
                H1('loading...')
            ).bind(listState, (el, state) => {
                if(!state) return;
                
                const tags = state.map(item => H2(item.name));

                return (
                    el.children(...tags)
                )
            })
        )
    )
}