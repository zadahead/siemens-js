import { Element } from "../element";

class InputElement extends Element {
    constructor(state) {
        super('input');

        this.state = state;
        this.onKeyUp(this.handleStateChange);

        return this.setValue();
    }

    getValue = () => {
        return this.state.value;
    }

    setValue = () => {
        this.elem.value = this.getValue();
        return this;
    }

    placeholder = (placeholder) => {
        return this.attr({ placeholder });
    }

    handleStateChange = () => {
        console.log(this.elem.value);
        this.state.setValue(this.elem.value);
    }

    onKeyUp = (fn) => {
        this.events({ keyup: fn})
    }
}

export const Input = (value) => new InputElement(value);