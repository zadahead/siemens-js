export class State {
    
    constructor (initialState) {
        this.value = initialState;
        this.subs = [];
    }

    setValue = (value) => {
        this.value = value;
        this.onChange();
    }

    subscribe = (fn) => {
        this.subs.push(fn);
    }

    onChange = () => {
        this.subs.forEach(func => {
            func(this.value);
        });
    }
}