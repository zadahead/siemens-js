
export class Element {
    elem = null;

    constructor(value) {
        const [tag, ...classes] = this.#split(value);

        this.elem = document.createElement(tag);
        this.elem.data = this;

        this.addClasses(classes);
       
        this.appendTo();
        
        return this;
    }

    #split = (value) => {
        return value.split('.');
    }

    content = (content) => {
        this.elem.innerHTML = content;
        return this;
    }

    addClass = (cls) => {
        if(cls) {
            this.elem.classList.add(cls);
        }
        
        return this;
    }

    addClasses = (classes) => {
        classes.forEach((cls) => {
            this.addClass(cls);
        })

        return this;
    }

    removeClass = (cls) => {
        if(cls) {
            this.elem.classList.remove(cls);
        }

        return this;
    }

    style = (style) => {
        Object.assign(this.elem.style, style);
        return this;
    }

    attr = (attr) => {
        Object.entries(attr).forEach(entry => {
            const [key, value] = entry;
            this.elem.setAttribute(key, value);
        });
        return this;
    }

    events = (attr) => {
        Object.entries(attr).forEach(entry => {
            const [event, func] = entry;
            this.elem.addEventListener(event, func);
        });
        return this;
    }

    children = (...list) => {
        if(Array.isArray(list[0])) {
            list = list[0];
        }
        
        this.empty();
        
        list.forEach((child) => {
            child.appendTo(this.elem);
        })

        return this;
    }

    empty = () => {
        while (this.elem.firstChild) {
            this.elem.firstChild.remove()
        }
    }
    
    appendTo = (parent = document.body) => {
        parent.append(this.elem);
        return this;
    }

    bind = (state, fn) => {
        state.subscribe((value) => fn(this, value));

        fn(this, state.value)
        return this;
    }
}

export const El = (tag) => new Element(tag);
