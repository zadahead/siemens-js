import { El } from '../element';

export const H1 = (content) => El('h1').content(content);
export const H2 = (content) => El('h2').content(content);