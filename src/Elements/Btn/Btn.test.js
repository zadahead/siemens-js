import { fireEvent } from '@testing-library/react';
import { Btn, Input, State } from '../';

test('This is a big test', () => {
    const title = 'Click Me';
    const handleClick = jest.fn();

    const box = Btn(title)(handleClick);
    expect(box.elem).toBeInTheDocument();
    expect(box.text()).toEqual(title);


    fireEvent.click(box.elem);

    expect(handleClick).toBeCalledTimes(1);
})

test('This will test inputs', () => {
    const state = new State('mo');
    const input = Input(state);

    expect(input.getValue()).toEqual('mo');

    fireEvent.change(input.elem, { target: { value: '23' } });

    expect(state.value).toEqual('23');
})