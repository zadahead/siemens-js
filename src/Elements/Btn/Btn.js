import { Element } from "../element";

import './Btn.css';

class BtnElement extends Element {
    constructor (content) {
        super('button');

        return this.addClass('Btn').content(content).onClick;

    }

    onClick = (ev) => {
        return this.events({ click: ev})
    }
}

export const Btn = (content) => new BtnElement(content); 