
import { Element } from '../element';

class DivElement extends Element {
    constructor (cls) {
        super('div');

        return this.addClass(cls).children;

    }
}

export const Div = (...args) => {
    const [cls] = args;
    
    if(typeof(cls) === 'string') {
        return new DivElement(cls);
    }
    
    return new DivElement()(...args)
};