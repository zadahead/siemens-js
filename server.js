require('dotenv').config();

const express = require('express');
const cors = require('cors');
const app = express();

app.use(cors({
    origin: '*'
}))

app.get('/users', (req, res) => {
    const users = [
        { 
            id: 1, name: "Mosh"
        },
        {
            id: 2, name: "David"
        }
    ]
    res.send(users);
})

app.listen(4000, () => {
    console.log('Server is listening on port 4000');
})